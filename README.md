
![Expensify App Logo](public/images/expensify_logo.svg)

#### [Live Demo](http://expensify.bkevans.com) - Be patient...this app may be slow to start due to being deployed on Heroku

An application a user can use to list, sort, and filter various expenses built with React and Redux. Authentication and data storage are both provided by Google Firebase. Styling is written in Sass and is built on top of the Materialize CSS framework. Webpack handles compiling the code into a browser-friendly bundle with the help of Babel and node-sass.   

## Getting Started

### Prerequisites

Make sure that you have [Node.js](https://nodejs.org/en/) and [Yarn](https://yarnpkg.com/en/) installed.

To check the versions you have installed, run:  

```
node -v && yarn -v
```

### Installing

First you'll need to clone the source files onto your local machine. 

To clone you can run:

```
git clone https://bkevans@bitbucket.org/bkevans/expensify-app.git
```

Once cloned, move into the app directory:

```
cd expensify-app/
```
Now you'll need to install of the necessary dependencies to run the app locally using Yarn: 

```
yarn install
```

With all of the dependencies installed, you'll need to provide a few environment variables for the app to sync with Firebase and run.

These variables can be placed in two separate *.env* files placed in the root directory of the project. 

One named: *.env.developement* for the development environment.

One named: *.env.testing* for running application tests.

These files each expect the same variables:

    FIREBASE_API_KEY=
		FIREBASE_AUTH_DOMAIN=
		FIREBASE_DATABASE_URL=
		FIREBASE_PROJECT_ID=
		FIREBASE_STORAGE_BUCKET=
		FIREBASE_MESSAGING_SENDER_ID= 

For more information on integrating applications with Google Firebase, visit the [firebase docs](https://firebase.google.com/docs/web/setup)

Once the environment variables are in place you're ready to run a development build and spin up a development server for the app by running: 

```
yarn dev-server
```

This will build the app, and run it locally. The output from this command will provide a URL where the app can be accessed in your browser, e.g. *http://localhost:8080/*

Now that the app is up and running locally, you can crack open the /src directory and play around with the code to update components or styling. Changes will be rendered in the browser immediately as long as the development server is running. 

To shut down the dev server, simply press [cntrl + c] 

## Application Testing

I have built both unit and integration tests for authentication, database CRUD operations and promise fulfilment, and component rendering/updating.

To run unit tests with Jest, make sure you have provided .env testing keys for Firebase and simply run:

```
 yarn test
```

## Deployment

This project uses the Heroku CLI for deploying the application to Heroku. If you'd like to host your own customized version of this app for free on Heroku, more information can be found in the [Heroku docs](https://devcenter.heroku.com/articles/how-heroku-works).

More information on git based deployments with the Heroku CLI can be found [here](https://devcenter.heroku.com/articles/heroku-cli). 

There is a script in *package.json* for deploying a production build to Heroku, which is utilized by the Heroku CLI. This is the **heroku-postbuild** script.

To create a production build of this app, stop your dev server (if it is still running [cntrl + c], and run:  

```
 yarn build:prod
```

A production build will create a new *bundle.js* file in the public directory of the app. 

## Built With

* [React](https://reactjs.org/) - The UI library used
* [Redux](https://reactjs.org/) - Used for application state management
* [Babel](https://babeljs.io/) - As a JavaScript compiler
* [Sass](https://sass-lang.com/) - To make things look interesting
* [Webpack](https://webpack.js.org/) - Used as a module bundler and dev-server
* [Google Firebase](https://firebase.google.com/) - Auth provider and NoSQL database
* [Materialize CSS](https://materializecss.com/) - A CSS framework built with Google's material design principles
* [react-materialize](https://react-materialize.github.io/#/) - A library providing React components built on the Materialize Framework
* [Jest](https://jestjs.io/) - The framework used for unit testing the application

## License

This project is licensed under the MIT License

## Acknowledgments

This project was initially built while participating in an excellent [Udemy course](https://www.udemy.com/react-2nd-edition/learn/v4/overview) provided by Andrew Mead. A huge thank you to Andrew for sharing his knowledge and helping others grow their developement toolkit!

