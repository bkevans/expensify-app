import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import numeral from 'numeral';

const ExpenseListItem = ({ id, description, amount, createdAt }) => (
    <Link className="expense-list__item" to={`/edit/${id}`}>
        <div>    
            <h5 className="list-item__description">{description}</h5>
            <span>{moment(createdAt).format('MMMM Do, YYYY')}</span>
        </div>
        <h5 className="list-item__amount">{numeral(amount / 100).format('$0,0.00')}</h5>
    </Link>
);

export default ExpenseListItem;

