import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { startLogout } from '../actions/auth'

export const Header = ({ startLogout }) => (
	<header className="header row">
		<div className="col s12">
		<div className="container header__content">	
			<div>
				<Link to="/dashboard">
					<h1 className="header__title">Expensify</h1>
				</Link>
				<p className="header__subtitle">Get your expenses under control</p> 
			</div>
			<button onClick={startLogout} className="btn-flat header__button">Logout</button>
		</div>
		</div>
	</header>
);

const mapDispatchToProps = (dispatch) => ({
	startLogout: () => dispatch(startLogout())
});

export default connect(undefined, mapDispatchToProps)(Header);