import React from 'react';
import { connect } from 'react-redux';
import { DateRangePicker } from 'react-dates';
import { setTextFilter, sortByDate, sortByAmount, setStartDate, setEndDate } from '../actions/filters';
import { Input } from 'react-materialize'

export class ExpenseListFilters extends React.Component {
    state = {
        calendarFocused: null
    };
    onDatesChange = ({ startDate, endDate }) => {
        this.props.setStartDate(startDate);
        this.props.setEndDate(endDate);
    };
    onFocusChange = (calendarFocused) => {
        this.setState(() => ({ calendarFocused }));
    };
    onTextChange = (e) => {
        this.props.setTextFilter(e.target.value);
    };
    onSortChange = (e) => {
        if (e.target.value === 'date') {
            this.props.sortByDate(e.target.value);
        } else if (e.target.value === 'amount') {
            this.props.sortByAmount(e.target.value);                
        }
    };
    render() {
        return (
            <div className="container filters-container">
                <div className="row input-wrapper">
                    <Input 
                        placeholder="Search Expenses"
                        className="material-text"
                        s={8}
                        m={4}
                        l={4} 
                        value={this.props.filters.text} 
                        onChange={this.onTextChange}
                    />
                    <Input 
                        s={4}
                        m={2}
                        l={2} 
                        type="select"
                        className="material-select" 
                        value={this.props.filters.sortBy} 
                        onChange={this.onSortChange}                    
                    >
                        <option value="date">Date</option>
                        <option value="amount">Amount</option>
                    </Input>

                    <div className="col s12 m6 l6">
                        <DateRangePicker
                            startDate={this.props.filters.startDate}
                            endDate={this.props.filters.endDate}
                            onDatesChange={this.onDatesChange}
                            focusedInput={this.state.calendarFocused}
                            onFocusChange={this.onFocusChange}
                            showClearDates={true}
                            numberOfMonths={1}
                            isOutsideRange={() => false}
                        />
                    </div>
                </div> 
            </div>
        );
    }
};

const mapStateToProps = (state) => ({
    filters: state.filters
});

const mapDispatchToProps = (dispatch) => ({
    setTextFilter: (text) => dispatch(setTextFilter(text)),
    sortByDate: () => dispatch(sortByDate()),
    sortByAmount: () => dispatch(sortByAmount()),
    setStartDate: (startDate) => dispatch(setStartDate(startDate)),
    setEndDate: (endDate) => dispatch(setEndDate(endDate))
});

export default connect(mapStateToProps, mapDispatchToProps)(ExpenseListFilters);