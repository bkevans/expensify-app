import React from 'react';
import moment from 'moment';
import { Input } from 'react-materialize'
import { SingleDatePicker } from 'react-dates';

export default class ExpenseForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            description: props.expense ? props.expense.description : '',
            note: props.expense ? props.expense.note : '',
            amount: props.expense ? (props.expense.amount/100).toString() : '',
            createdAt: props.expense ? moment(props.expense.createdAt) : moment(),
            calendarFocused: false
        }
    };
    onDescriptionChange = (e) => {
        const description = e.target.value;
        this.setState(() => ({ description }));
    };
    onNoteChange = (e) => {
        const note = e.target.value;
        this.setState(() => ({ note }));
    };
    onAmountChange = (e) => {
        const amount = e.target.value;
        if (!amount || amount.match(/^\$?([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}[0-9]{0,}(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$/)) {
            this.setState(() => ({ amount }));
        }
    };
    onDateChange = (createdAt) => {
        if (createdAt) {
            this.setState(() => ({ createdAt }));
        }
    }; 
    onFocusChange = ({ focused }) => {
        this.setState(()=> ({ calendarFocused: focused }));
    };
    onSubmit = (e) => {
        e.preventDefault();
        if (!this.state.description && (!this.state.amount || this.state.amount === '0')) {
            this.setState(() => ({'error': 'Please provide a description and amount.'}));
        } else if (!this.state.description) {
            this.setState(() => ({'error': 'Please provide a description for this expense.'}));
        } else if (!this.state.amount) {
            this.setState(() => ({'error': 'Please provide an amount for this expense.'}));
        } else if (this.state.amount === '0') {
            this.setState(() => ({'error': 'An expense with an amount of 0 is actually a gift.'}));
        } else {
            this.setState(() => ({'error': ''}));
            this.props.onSubmit({
                description: this.state.description,
                amount: parseFloat(this.state.amount, 10) * 100,
                createdAt: this.state.createdAt.valueOf(),
                note: this.state.note
            });
        }
    };
    render() {
        return (
            <form onSubmit={this.onSubmit}> 
                {this.state.error && <p className="expense-form__error">{this.state.error}</p>}
                <Input 
                    label="Description"
                    autoFocus
                    value={this.state.description}
                    onChange={this.onDescriptionChange}
                />
                <Input
                    label="Amount"
                    value={this.state.amount}
                    onChange={this.onAmountChange}
                />
                <div className="input-wrapper">
                    <label className="date-label">Date</label>
                    <SingleDatePicker 
                        date={this.state.createdAt}
                        onDateChange={this.onDateChange}
                        focused={this.state.calendarFocused}
                        onFocusChange={this.onFocusChange}
                        numberOfMonths={1}
                        isOutsideRange={() => false}
                    />
                </div>
                <div className="input-field col">
                <textarea
                    value={this.state.note}
                    onChange={this.onNoteChange}
                    className="materialize-textarea"
                    id="expense-text"
                >
                </textarea>
                <label htmlFor="expense-text" className={this.state.note.length > 0 ? 'active' : ''}>Add a note for this expense (optional)</label>
                </div>
                <button className="btn waves-effect waves-light expense-form__button">{this.props.expense ? 'Save Expense' : 'Add Expense' }</button>
            </form>
        )
    }
};
