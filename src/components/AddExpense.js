import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ExpenseForm from './ExpenseForm';
import { startAddExpense } from '../actions/expenses';

export class AddExpense extends React.Component {
	onSubmit = (expense) => {
		this.props.startAddExpense(expense);
		this.props.history.push('/dashboard');
	};
	render() {
		return (
			<div>
				<div className="page-header">	
					<div className="container row page-header__container">
						<div className="col s12 page-header__centered">
							<h2 className="page-header__title">Add an Expense</h2>
						</div>
					</div>
				</div>
				<div className="container expense-form__container">
					<ExpenseForm 
						onSubmit={this.onSubmit}
					/>
					<Link className="btn-flat expense-form__secondary-button" to="/dashboard">cancel</Link>
				</div>
			</div>	
		);
	}
}

const mapDispatchToProps = (dispatch) => ({
	startAddExpense: (expense) => dispatch(startAddExpense(expense))
});

export default connect(undefined, mapDispatchToProps)(AddExpense);