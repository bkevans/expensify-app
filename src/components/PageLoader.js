import React from 'react';
import { Preloader } from 'react-materialize';

const PageLoader = () => (
    <div className="page-loader">
        <Preloader size='big' color="red"/>
    </div>
);

export default PageLoader;