import React from 'react';
import { connect } from 'react-redux';
import { startLogin } from '../actions/auth';

export const LoginPage = ({ startLogin }) => (
    <div className="box-layout">
        <div className="box-layout__box z-depth-4">
            <h1 className="box-layout__title">Expensify</h1>
            <p className="box-layout__subtitle">Get your expenses under control</p> 
            <button className="btn box-layout__button" onClick={startLogin}>Login with google</button>
        </div>
    </div>
);

const mapDispatchToProps = (dispatch) => ({
    startLogin: () => dispatch(startLogin())
});

export default connect(undefined, mapDispatchToProps)(LoginPage);