import React from 'react';
import { shallow } from 'enzyme';
import PageLoader from '../../components/PageLoader';

test('should render PageLoader correctly', () => {
    const wrapper = shallow(<PageLoader />);
    expect(wrapper).toMatchSnapshot();
});
